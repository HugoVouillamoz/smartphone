import Contacts.Contact;
import Errors.SmartphoneException;
import Gallery.addPicture;
import Meteo.WeatherAPIRequest;
import org.junit.jupiter.api.Test;

import javax.swing.*;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SmartphoneTest{


    @Test
   void testEmptyFirstName(){

        int errorCode = 0;
        try {
         Contact myContact = new Contact("    ", "Vouillamoz", "078 975 42 58", "img/DefaultContactImage.png");
        }catch (SmartphoneException error)  {
            errorCode=error.getErrorCode();
        }
        assertEquals(100,errorCode);
    }

    @Test
    void testEmptyLastName(){
        int errorCode = 0;
        try{
            Contact myContact = new Contact("Hugo", "      ", "078 526 24 30", "img/DefaultContactImage.png");
        }catch (SmartphoneException error){
            errorCode=error.getErrorCode();
        }
        assertEquals(101, errorCode);
    }

    @Test
    void testEmptyTelNumber(){
        int errorCode = 0;
        try{
            Contact myContact = new Contact("Hugo", "Vouillamoz", "", "img/DefaultContactImage.png");
        }catch (SmartphoneException error){
            errorCode=error.getErrorCode();
        }
        assertEquals(102, errorCode);
    }

    @Test
    void testInvalidTelNumber(){
        int errorCode = 0;
        try{
            Contact myContact = new Contact("Hugo", "Vouillamoz", "78 569 42 05", "img/DefaultContactImage.png");
        }catch (SmartphoneException error){
            errorCode=error.getErrorCode();
        }
        assertEquals(103, errorCode);
    }

    @Test
    void testContactValide(){
        int errorCode = 0;
        try{
            Contact myContact = new Contact("Hugo", "Vouillamoz", "078 569 52 02", "img/DefaultContactImage.png");
        }catch(SmartphoneException error){
            errorCode = error.getErrorCode();
        }
        assertEquals(0, errorCode);
    }
    @Test
    void testInvalidCity(){
        int errorCode=0 ;

        try{
            WeatherAPIRequest.apiRequest(".");
        }catch (SmartphoneException error){
            errorCode=error.getErrorCode();
        } catch (IOException e) {
            errorCode=202;
        } catch (URISyntaxException e) {
            errorCode=203;
        }
        assertEquals(201, errorCode);
    }

    @Test
    void testEmptyCity(){
        int errorCode=0 ;

        try{
            WeatherAPIRequest.apiRequest("");
        }catch (SmartphoneException error){
            errorCode=error.getErrorCode();
        } catch (IOException e) {
            errorCode=202;
        } catch (URISyntaxException e) {
            errorCode=203;
        }
        assertEquals(200, errorCode);
    }

    @Test
    void testExtension(){
        int errorCode = 0;
    try {
        addPicture.validationPicture("img/test.txt");
    }catch (SmartphoneException error){
        errorCode = error.getErrorCode();
    }
        assertEquals(300, errorCode);
    }

    @Test
    void testPathValidity(){
        int errorCode = 0;
        try {
            addPicture.validationPicture("img/hello.jpg");
        }catch (SmartphoneException error){
            errorCode = error.getErrorCode();
        }
        assertEquals(301, errorCode);
    }

    @Test
    void testPathEmpty(){
        int errorCode = 0;
        try {
            addPicture.validationPicture("");
        }catch (SmartphoneException error){
            errorCode = error.getErrorCode();
        }
        assertEquals(305, errorCode);
    }

}
