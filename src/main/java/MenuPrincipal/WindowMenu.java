package MenuPrincipal;

import Contacts.ContactWindow;
import Contacts.allContactWindow;
import Gallery.GalleryWindow;
import Meteo.MeteoWindows;
import Smartphone.SmartphoneShape;

import javax.imageio.stream.ImageInputStream;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

/**
 * Panel utilisé pour afficher le menu principal
 */
public class WindowMenu extends JPanel {


    private   JButton app1;
    private   ImageIcon imageApp1;
    private   JButton app2 ;
    private   ImageIcon imageApp2;
    private   JButton app3;
    private   ImageIcon imageApp3;
    private   Image background = new ImageIcon(ClassLoader.getSystemResource("img/Background.png")).getImage();

    /**
     * Création du menu principal avec 3 bouton contenant les 3 applications différentes
     * Chaque bouton a une image conernant son application
     */
    public WindowMenu(){

       imageApp1 = new ImageIcon(new ImageIcon(ClassLoader.getSystemResource("img/ContactApplicationIcone.png")).getImage().getScaledInstance(60,60,Image.SCALE_SMOOTH));
       app1 = new JButton(imageApp1);
       app1.setBorderPainted(false);
       app1.setContentAreaFilled(false);

       imageApp2 = new ImageIcon(new ImageIcon(ClassLoader.getSystemResource("img/meteoApplication.png")).getImage().getScaledInstance(50,50,Image.SCALE_SMOOTH));
       app2 = new JButton(imageApp2);
       app2.setBorderPainted(false);
       app2.setContentAreaFilled(false);

       imageApp3 = new ImageIcon(new ImageIcon(ClassLoader.getSystemResource("img/galeryApplication.png")).getImage().getScaledInstance(50,50,Image.SCALE_SMOOTH));
       app3 = new JButton(imageApp3);
       app3.setBorderPainted(false);
       app3.setContentAreaFilled(false);

       app1.addActionListener(new MenuButtonListener());
       app2.addActionListener(new MenuButtonListener());
       app3.addActionListener(new MenuButtonListener());

       setLayout(new FlowLayout(FlowLayout.CENTER,10,75));
       add(app1);
       add(app2 );
       add(app3);
       setOpaque(false);

    }

    /**
     * Méthode utilisée pour dessiner le fond d'écran du menu principal
     */
    public void paintComponent( Graphics g){
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.drawImage(background,0,0,350,750, null);
    }


    /**
     * Création des listeners pour les 3 bouton d'application
     * chaque bouton permet de créer un panel principal de son application qui viens remplacer le panel menu principal dans le card layout
     */
    public class MenuButtonListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e){
            if(e.getSource() == app1 ){
                SmartphoneShape.changeApplication(new ContactWindow());
            }else if (e.getSource() == app2){
                SmartphoneShape.changeApplication(new MeteoWindows());

            }else if(e.getSource() == app3){
                SmartphoneShape.changeApplication(new GalleryWindow());
            }
        }
    }
}
