package Errors;

import java.net.URISyntaxException;

/**
 * Création d'une class d'exception personnel qu'on utilise pour ensuite afficher le message d'erreur voulu
 */
public class SmartphoneException extends Exception {

    private int errorCode;

    public SmartphoneException(int errorCode){
        this.errorCode = errorCode;
    }

    public int getErrorCode(){
        return errorCode;
    }
}
