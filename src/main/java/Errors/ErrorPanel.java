package Errors;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.RoundRectangle2D;

/**
 * Fenêtre JDialog utilisée pour afficher le message d'erreur souhaité
 */
public class ErrorPanel extends JDialog{

    JPanel panel = new JPanel();
    JButton bouton = new JButton("OK");
    JLabel messageError = new JLabel("");
    Font myFont = new Font("ARIAL", Font.BOLD, 16);

    private Dimension SIZE = new Dimension(350,750);
    private RoundRectangle2D.Double SHAPE = new RoundRectangle2D.Double(0,0,350,750,90,90);

    /**
     * Création de la fenêtre JDialogue qui va venir recouvrir tout l'écran du smartphone
     * Elle contient le texte d'erreur voulu ainsi qu'un bouton pour continuer
     * @param errorCode code de l'erreur qui à été créé
     */
    public ErrorPanel( int errorCode){

        this.setPreferredSize(SIZE);
        setUndecorated(true);
        setShape(SHAPE);
        setVisible(true);

        setOpacity(0.90f);

        bouton.addActionListener(new ErrorPanelListener());
        bouton.setBorderPainted(false);
        bouton.setBackground(new Color(255, 255, 255));

        panel.setLayout(new GridLayout(7,1));
        panel.setPreferredSize(SIZE);
        panel.setBackground(new Color(54, 51, 51));
        panel.add(new JLabel());
        panel.add(new JLabel());
        panel.add(messageError, BorderLayout.CENTER);
        panel.add(new JLabel());
        panel.add(bouton, BorderLayout.SOUTH);

        add(panel);

        messageError.setFont(myFont);
        messageError.setForeground(new Color(255, 255, 255));
        setMessageError(errorCode);

        pack();
        setModal(true);

        setLocationRelativeTo(null);

    }

    /**
     * Méthode utilisant un switch afin de mettre le bon message d'erreur dans le JDialog
     * Les erreurs ont été séparé par application (100 = contact , 200 météo , 300 gallery)
     * on peut ainsi facilement créer de nouvelle erreur ou modifié le text d'une erreur spécifique
     * @param errorCode
     */

    public void setMessageError(int errorCode){

        switch(errorCode){
            case 100:
                messageError.setText("le prénom ne peut pas être vide");
                break;
            case 101:
                messageError.setText("le nom ne peut pas être vide");
                break;
            case 102:
                messageError.setText("le numéro de téléphone ne peut pas être vide");
                break;
            case 103:
                messageError.setText("le numéro de téléphone n'est pas valide");
                break;
            case 104:
                messageError.setText("Ce numéro est déjà attribué à un contact");
                break;
            case 200:
                messageError.setText("Le lieu est vide !");
                break;
            case 201:
                messageError.setText("Le lieu n'existe pas");
                break;
            case 202:
                messageError.setText("La requête a échoué");
                break;
            case 203:
                messageError.setText("Aucune données n'a été trouvé");
                break;
            case 300:
                messageError.setText("Erreur extension. le chemin choisi n'est pas une image");
                break;
            case 301:
                messageError.setText("Le chemin choisi n'existe pas");
                break;
            case 302:
                messageError.setText("Json corrompus");
                break;
            case 303:
                messageError.setText("L'image est déjà présente dans les favoris");
                break;
            case 304:
                messageError.setText("L'image est déjà présente dans la galerie");
                break;
            case 305:
                messageError.setText("Le champ 'Ajouter une image' est vide");
                break;
        }
    }

    /**
     * Permet de faire disparaitre le JDialog une fois le bouton appuyé
     */
    public void removePanel(){
        setVisible(false);
    }


    /**
     * Listener utlisé pour enlever le JDialog d'erreur
     */

    public class ErrorPanelListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e){

            if(e.getSource() == bouton){
                removePanel();
            }
        }
    }
}
