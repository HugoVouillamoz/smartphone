package Meteo;
import javax.swing.*;
import java.awt.*;

/**Création de la classe des données météos et impression de ces données dans les labels dédiées a être appelé dans
 * l'affichage
 */

public class DataPrinter extends JPanel{

    //Constructeur de l'objet DataPrinter
    //------------------------------------------------------------------------------------------------------------------

    public DataPrinter(){

        setOpaque(false);

        JLabel dateLabel = new JLabel();
        JLabel tempMaxLabel = new JLabel();
        JLabel tempMinLabel = new JLabel();
        JLabel precipitationLabel = new JLabel();
        JLabel sourceLabel= new JLabel();

        dateLabel.setText("Date: "+ APIResults.date);
        tempMaxLabel.setText(APIResults.maxtemp+"°C");
        tempMinLabel.setText(APIResults.mintemp+"°C");
        precipitationLabel.setText("Précipitation: "+APIResults.pop);
        sourceLabel.setText("Source: "+APIResults.source);

        dateLabel.setFont(new Font("Arial", Font.BOLD, 12));
        tempMaxLabel.setFont(new Font("Arial", Font.BOLD, 26));
        tempMinLabel.setFont(new Font("Arial", Font.BOLD, 14));
        precipitationLabel.setFont(new Font("Arial", Font.BOLD, 12));
        sourceLabel.setFont(new Font("Arial", Font.BOLD, 10));

        add(dateLabel);
        add(tempMaxLabel);
        add(tempMinLabel);
        add(precipitationLabel);
        add(sourceLabel);
    }
}
