package Meteo;
import org.json.JSONArray;
import org.json.JSONObject;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

/** Impressions des données météos envoyé par l'API. Classe
 */

public class APIResults {

    //Déclaration des attributs
    //------------------------------------------------------------------------------------------------------------------

    public static double maxtemp;                                                                                       //déclaration de la valeur de température maximale
    public static double mintemp;                                                                                       //déclaration de la valeur de température minimale
    public static double pop;                                                                                           //déclaration de la valeur de précipitation
    public static String source;                                                                                        //déclaration de la source des données
    public static String date;                                                                                          //déclaration de la date des prévisions
    private static String cityResponse;                                                                                 //déclaration du nom de la ville retourné par l'API

    //Impressions des resultats des résultats
    //------------------------------------------------------------------------------------------------------------------

    public static void printResults(String rawResult) {

        //Envoie des resulats de requetes
        //---------------------------------------------------------

        if (rawResult==null || rawResult.isEmpty()) {                                                                   //Si les resultats sont vides ou si il n'y a pas de données
            System.out.printf("No raw data%n");
            return; }

        JSONObject printResults = new JSONObject(rawResult);
        ZoneId zoneId=ZoneId.of(printResults.getString("timezone"));

        cityResponse=printResults.getString("resolvedAddress");
        JSONArray values=printResults.getJSONArray("days");

        //Envoie des resulats de requetes
        //---------------------------------------------------------

        ScrollPane.allResultsPanel.removeAll();
        for (int i = 0; i < values.length(); i++) {

            JSONObject dayValue = values.getJSONObject(i);
            ZonedDateTime datetime=ZonedDateTime.ofInstant(Instant.ofEpochSecond
                    (dayValue.getLong("datetimeEpoch")), zoneId);

            //Envoie des resulats de requetes
            //---------------------------------------------------------

            maxtemp=dayValue.getDouble("tempmax");
            mintemp=dayValue.getDouble("tempmin");
            pop=dayValue.getDouble("precip");
            source=dayValue.getString("source");
            date=datetime.format(DateTimeFormatter.ISO_LOCAL_DATE);

            //Envoie des resulats de requetes
            //---------------------------------------------------------

            MeteoWindows.cityPrint.setText("Voici les prévisions pour: ");
            MeteoWindows.cityResponseLabel.setText(cityResponse);
            MeteoWindows.setRealPrecipitation(pop);

            AllResultsPrinter print1 = new AllResultsPrinter();
        }
    }
}
