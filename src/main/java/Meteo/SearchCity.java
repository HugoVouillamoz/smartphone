package Meteo;

import Errors.ErrorPanel;
import Errors.SmartphoneException;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URISyntaxException;

/** Cette classe permet de créer un object de recherche avec la ville que l'utilisateur a entrée dans le textField.
 * Une fois la ville entré, la classe WeatherAPIrequest va être appelé afin de créer la requête.
 */

public class SearchCity implements ActionListener {

    //Action du bouton validation
    //---------------------------------------------------------

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == MeteoWindows.validation) {
            String city = MeteoWindows.textField.getText();

            try {
                    WeatherAPIRequest.apiRequest(city);
                    MeteoWindows.scroll1.setVisible(true);

                } catch (SmartphoneException errorSmartphone) {
                    new ErrorPanel(errorSmartphone.getErrorCode());

                } catch (IOException IOError) {
                   new ErrorPanel(202);

                } catch (URISyntaxException URIError) {
                    new ErrorPanel(203);
            }
        }
    }
}

