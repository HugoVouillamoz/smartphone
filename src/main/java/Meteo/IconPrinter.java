package Meteo;
import javax.swing.*;
import java.awt.*;

/**Déclaration et choix des icônes météos en fonctions des donnés de précipitation envoyées par l'API
 */

public class IconPrinter extends JPanel {

    //Déclaration des attributs
    //------------------------------------------------------------------------------------------------------------------

    private ImageIcon imageSun =                                                                                        //Déclaration de l'icone "soleil"
            new ImageIcon(new ImageIcon(ClassLoader.getSystemResource("img/sun1.png"))
                    .getImage().getScaledInstance(100,100,Image.SCALE_DEFAULT));
    private ImageIcon imageMiddle =                                                                                     //Déclaration de l'icone "nuageux"
            new ImageIcon(new ImageIcon(ClassLoader.getSystemResource("img/middle1.png"))
                    .getImage().getScaledInstance(100,100,Image.SCALE_DEFAULT));
    private ImageIcon imageRain =                                                                                       //Déclaration de l'icone "pluie"
            new ImageIcon(new ImageIcon(ClassLoader.getSystemResource("img/rain1.png"))
                    .getImage().getScaledInstance(100,100,Image.SCALE_DEFAULT));

    //Constructeur de l'objet IconPrinter
    //------------------------------------------------------------------------------------------------------------------

    public IconPrinter(double precipitationValue){

        setOpaque(false);

        if(precipitationValue==0.0){                                                                                    //Si la valeur est à 0.0, nous affichons un soleil
            add(new JLabel(imageSun));
        }
        if(precipitationValue>0.0 && precipitationValue<1.0){                                                           //Si la valeur se situe entre 0.0 et 1.0 nous affichons un temps nuageux
            add(new JLabel(imageMiddle));
        }
        if(precipitationValue>1.0){                                                                                     //Si la valeur se situe en dessus de 1.0, nous affichons un nuage pluvieux
            add(new JLabel(imageRain));
        }
    }
}
