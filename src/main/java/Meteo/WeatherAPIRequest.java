package Meteo;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;

import Errors.SmartphoneException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

/** Cette classe permet d'effectuer la requête vers l'API de météo Weather Visual Crossing. Le but est de créer
 * l'url de requête en y intégrant la ville recherché par l'utilisateur. Puis de tester si il y'a une réponse de l'API
 * ou non.
 */

public class WeatherAPIRequest {

    //Requete de recherche des données
    //------------------------------------------------------------------------------------------------------------------

    public static void apiRequest(String city) throws SmartphoneException, IOException, URISyntaxException {

        //Configuration du lieu de requête
        //---------------------------------------------------------

        if(city.isEmpty()){
            throw new SmartphoneException(200);
        }

        String apiEndPoint="https://weather.visualcrossing.com/VisualCrossingWebServices/rest/services/timeline/"+city  //Ajout de la valeur "city" dans la requête
                +"/next4days?unitGroup=metric&key=B5XP2QFKPD4SZEUAWW8VNLLRA";

        StringBuilder requestBuilder=new StringBuilder(apiEndPoint);                                                    //Construction d'une String avec le contenu de apiEndPoint

        URIBuilder builder = new URIBuilder(requestBuilder.toString());                                                 //COnstruction d'une URl avec le String requestBuilder

        HttpGet get = new HttpGet(builder.build());                                                                     //Mise en place du protocole Http

        CloseableHttpClient httpclient = HttpClients.createDefault();

        CloseableHttpResponse response = httpclient.execute(get);                                                       //exécution de la requête

        String rawResult=null;

        try {
            if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK){
                throw new SmartphoneException(201);
            }

            HttpEntity entity = response.getEntity();

            if (entity != null) {
                rawResult=EntityUtils.toString(entity, Charset.forName("utf-8"));
            }
        }
        finally {
            response.close();
        }
        APIResults.printResults(rawResult);
    }
}
