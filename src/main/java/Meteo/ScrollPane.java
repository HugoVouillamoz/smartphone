package Meteo;

import javax.swing.*;
import java.awt.*;

/** Déclaration du ScrollPan et affichage des résultats à l'intérieur.
 */

public class ScrollPane extends JPanel {

    //Déclaration des attributs
    //------------------------------------------------------------------------------------------------------------------

    private JScrollPane jScrollPane = new JScrollPane();                                                                //déclaration du JscrollPane
    public static JPanel allResultsPanel = new JPanel();                                                                //déclaration du panel pour affichaer les résultats
    private GridLayout allResultsLayout = new GridLayout(5,2,7,7);                                 //déclaration du Grid layout des resultats

    //Constructeur de l'objet ScrollPane
    //------------------------------------------------------------------------------------------------------------------

    public ScrollPane() {

        setLayout(new CardLayout());

        allResultsPanel.setOpaque(false);
        allResultsPanel.setPreferredSize(new Dimension(320,730));
        allResultsPanel.setLayout(allResultsLayout);

        jScrollPane.getVerticalScrollBar().setUnitIncrement(20);
        jScrollPane.getVerticalScrollBar().setVisible(false);

        jScrollPane.setWheelScrollingEnabled(true);
        jScrollPane.setViewportView(this);
        jScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        jScrollPane.getViewport().add(allResultsPanel);
        jScrollPane.setOpaque(false);
        jScrollPane.setPreferredSize(new Dimension(350,450));

        add(jScrollPane);
    }
}
