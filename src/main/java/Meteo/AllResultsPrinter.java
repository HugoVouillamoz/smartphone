package Meteo;

/**Impression de toutes les données requêtés par l'API
 */

public class AllResultsPrinter {

    //Constructeur de l'objet ALLResultsPrinter
    //------------------------------------------------------------------------------------------------------------------

    public AllResultsPrinter(){
        print();
    }

    //Impressions des objets donnés et icônes
    //------------------------------------------------------------------------------------------------------------------

    public void print(){
        ScrollPane.allResultsPanel.add(new DataPrinter());                                                              //Création de l'object Data printer (=impression des donnée météos=
        ScrollPane.allResultsPanel.add(new IconPrinter(MeteoWindows.realPrecipitation));                                //Création de l'object IconPrinter (affichage des icones)
    }
}

