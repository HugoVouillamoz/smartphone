package Meteo;

import javax.swing.*;
import java.awt.*;

/**Création de la fenêtre de l'application et affichage des différents panels à l'intérieur. Cette fenêtre se créer
 * lorsque l'utilisateur va séléctionner l'icône météo sur le menu principal.
 */

public class MeteoWindows extends JPanel {

    //Déclaration des attributs
    //------------------------------------------------------------------------------------------------------------------

    private JPanel titlePanel = new JPanel();                                                                           //Déclaration du panel du titre de l'app
    private JPanel texteFieldPanel = new JPanel();                                                                      //Déclaration du panel du textField
    private JPanel validationPanel = new JPanel();                                                                      //Déclaration du panel pour le bouton de validation
    private JPanel cityPanel = new JPanel();                                                                            //Déclaration du panel pour afficher la ville choisie

    private GridLayout titleLayout = new GridLayout(1, 1);                                                    //Déclaration du layout de titre
    private GridLayout textFieldLayout = new GridLayout(2, 2);                                                //Déclaration du layout pour le textField
    private GridLayout validationLayout = new GridLayout(1, 1);                                               //Déclaration du layout du bouton validation
    private GridLayout cityLayout = new GridLayout(2, 1);                                                     //Déclaration du layout pour l'affichage de la ville

    public static JLabel cityPrint = new JLabel();                                                                      //Déclaration du label d'affichage du nom de la ville
    public static JLabel cityResponseLabel = new JLabel();
    private JLabel enterCityText = new JLabel("Veuillez entrer une ville: ");                                       //Déclaration du label de libellé du textField
    private JLabel titleApp = new JLabel("              Meteo HES-SO");                                             //Déclaration du label du titre de l'app

    public static JTextField textField = new JTextField();                                                              //Déclaration du textField pour entrer un choix de ville

    private Image background = new ImageIcon(ClassLoader.getSystemResource("img/paysage.jpg")).getImage();        //Déclaration de l'image de BackGround

    public static JButton validation = new JButton("Afficher les prévisions");                                     //Déclaration du bouton de validation du choix

    public static double realPrecipitation;                                                                             //Déclaration de la variable des données de précipitations

    private Dimension titleSize = new Dimension(350, 40);                                                   //Déclaration des dimensions du panel de titre
    private Dimension textFieldSize = new Dimension(350, 60);                                               //Déclaration des dimensions du panel du textField
    private Dimension validationSize = new Dimension(350, 40);                                              //Déclaration des dimensions du panel du bouton de validation
    private Dimension printCitySize = new Dimension(310, 40);                                               //Déclaration des dimensions du panel d'affichage du nom de la ville choisie

    public static ScrollPane scroll1 = new ScrollPane();                                                                //Déclaration du ScrollPane

    //Constructeur de l'object MeteoWindows
    //------------------------------------------------------------------------------------------------------------------

    public MeteoWindows() {

        //Titre de l'App
        //---------------------------------------------------------

        titlePanel.setOpaque(false);
        titlePanel.setPreferredSize(titleSize);
        titlePanel.setLayout(titleLayout);
        titleApp.setFont(new Font("Arial", Font.BOLD, 22));
        titlePanel.add(titleApp, BorderLayout.CENTER);
        titlePanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        //TextField
        //---------------------------------------------------------

        texteFieldPanel.setOpaque(false);
        texteFieldPanel.setPreferredSize(textFieldSize);
        texteFieldPanel.setLayout(textFieldLayout);
        enterCityText.setFont(new Font("Arial", Font.BOLD, 10));
        texteFieldPanel.add(enterCityText, BorderLayout.NORTH);
        texteFieldPanel.add(textField, BorderLayout.CENTER);
        texteFieldPanel.setBorder(BorderFactory.createEmptyBorder(5, 20, 5, 20));

        //Bouton Validation
        //---------------------------------------------------------

        validationPanel.setOpaque(false);
        validationPanel.setPreferredSize(validationSize);
        validationPanel.setLayout(validationLayout);
        validationPanel.add(validation, BorderLayout.SOUTH);
        validationPanel.setBorder(BorderFactory.createEmptyBorder(0, 20, 5, 20));
        validation.setBackground(Color.white);
        validation.addActionListener(new SearchCity());

        //Nom de la ville
        //---------------------------------------------------------

        cityPrint.setText("");
        cityResponseLabel.setText("");
        textField.setText("");
        cityPanel.setOpaque(false);
        cityPanel.setPreferredSize(printCitySize);
        cityPanel.setLayout(cityLayout);
        cityPanel.add(cityPrint, BorderLayout.NORTH);
        cityPanel.add(cityResponseLabel,BorderLayout.SOUTH);
        cityResponseLabel.setFont(new Font("Arial", Font.BOLD,16));

        //Aujout des panels dans le panel MeteoWindows
        //---------------------------------------------------------

        add(titlePanel);
        add(texteFieldPanel);
        add(validationPanel);
        add(cityPanel);
        add(scroll1);
        scroll1.setVisible(false);
    }

    //Setter de la variable des précipitations
    //---------------------------------------------------------

    public static void setRealPrecipitation(double realPrecipitation) {
        MeteoWindows.realPrecipitation = realPrecipitation;
    }

    //Affichage du Background de l'app
    //---------------------------------------------------------

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.drawImage(background, -300, -10, 1000, 750, null);
    }
}












