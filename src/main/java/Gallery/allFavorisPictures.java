package Gallery;

import Errors.SmartphoneException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class allFavorisPictures extends allPictureWindow{
    /**
     * cette classe permet d'afficher la galerie d'image.
     * le contructeur de "allPictureWindow" contient la galerie d'image et la scrollbar qui permet de voir toutes les images.
     */
    public allFavorisPictures(String sourcePath) throws SmartphoneException {
        super(sourcePath);

    }


    @Override
    public void setListener(){
        for(int i = 0; i < pictureBtnArray.length; i++){
            pictureBtnArray[i].addActionListener(new viewFavorisPicture(arrayListPath.get(i)));
        }
    }

}

/**
 * l'action appelle la méthode qui ouvre l'image des favoris en plein écran
 */
class viewFavorisPicture implements ActionListener{


    public String path;

    public viewFavorisPicture(String path){
        this.path=path;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        GalleryWindow.openFavorisPicture(path);
    }
}


