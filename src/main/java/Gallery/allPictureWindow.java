package Gallery;

import Errors.ErrorPanel;
import Errors.SmartphoneException;
import SaveAndLoad.JSONStorageGallery;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.util.ArrayList;

public class allPictureWindow extends JPanel {


    protected JButton pictureBtnArray[]  ;
    public static ArrayList<String> arrayListPath = new ArrayList<String>();
    private JPanel btnPanel = new JPanel();
    private JPanel allPicturePanel = new JPanel();
    private JScrollPane scrollPicture = new JScrollPane();
    private JButton addPictureButton = new JButton("ajouter");

    /**
     * cette classe permet d'afficher la galerie d'image.
     * le contructeur de "allPictureWindow" contient la galerie d'image et la scrollbar qui permet de voir toutes les images.
     */
    public allPictureWindow(String sourcePath) throws SmartphoneException {

        //chargement de la galerie
        loadGallery(sourcePath);

        allPicturePanel.setLayout(new GridLayout(nbRowGallery(), 3,  1, 1));
        allPicturePanel.setPreferredSize(new Dimension(350, nbRowGallery() * 150));


        setLayout(new CardLayout());

        btnPanel.add(addPictureButton,BorderLayout.CENTER);

        //adapte le nombre de bouton au nombre d'image
        pictureBtnArray = new JButton[arrayListPath.size()];

        // la boucle for crée un bouton pour chaque image présent dans la array liste
          for(int i = 0; i < arrayListPath.size(); i++){

              try{
                  JButton newButton = new JButton(new ImageIcon(new ImageIcon(ClassLoader.getSystemResource(arrayListPath.get(i))).getImage().getScaledInstance(120,150,Image.SCALE_SMOOTH)));
                  newButton.setOpaque(false);
                  newButton.setContentAreaFilled(false);
                  newButton.setBorderPainted(false);
                  pictureBtnArray[i] = newButton;
                  allPicturePanel.add(newButton);
              }catch (NullPointerException error){
                  throw new SmartphoneException(302);
              }

            // si le chemin de l'image n'existe pas, on lance une exception

        }

        setListener();

        // Création de la scrollbar de la galerie
        scrollPicture.setPreferredSize(new Dimension(350,535));
        scrollPicture.getVerticalScrollBar().setUnitIncrement(20);
        scrollPicture.setWheelScrollingEnabled(true);
        scrollPicture.setViewportView(this);
        scrollPicture.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPicture.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        scrollPicture.getViewport().add(allPicturePanel);
        add(scrollPicture);

        add(btnPanel,BorderLayout.SOUTH);

        }


    /**
     *  création d'un action listener par bouton (pour chaque image)
     */
        public void setListener(){
            for(int i = 0; i < pictureBtnArray.length; i++){
                pictureBtnArray[i].addActionListener(new viewPicture(arrayListPath.get(i)));
            }
        }

    /**
     * calcule le nombre de ligne nécessaire par apport aux nombres images
     * @return
     */
        public static int nbRowGallery(){

        int nbRow;

            if(arrayListPath.size()%3==0){
                nbRow= arrayListPath.size()/3;
            }else{
                nbRow=arrayListPath.size()/3+1;
            }
            return nbRow;
        }

        public static void loadGallery(String sourceDestination){
                arrayListPath = JSONStorageGallery.read(new File(System.getenv("Smartphone") + sourceDestination), arrayListPath);

        }

        public static void saveGallery(String destinationPath){
        JSONStorageGallery.write(new File(System.getenv("Smartphone")+destinationPath),arrayListPath);
        }

}
