package Gallery;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * l'action appelle la méthode qui ouvre l'image en plein écran
 */
public class viewPicture implements ActionListener {

    public String path;

    public viewPicture(String path){
        this.path=path;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        GalleryWindow.openPicture(path);
    }


}


