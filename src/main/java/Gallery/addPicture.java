package Gallery;

import Errors.ErrorPanel;
import Errors.SmartphoneException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import static Gallery.allPictureWindow.arrayListPath;
import static Gallery.allPictureWindow.saveGallery;



public class addPicture extends JPanel {


    private JButton validationButton = new JButton("Valider");
    public static JTextField pathPicture = new JTextField(50);
    private JLabel txtUser = new JLabel("Ajouter une image");
    private JPanel addPathPanel = new JPanel();

    /**
     * cette classe permet d'ajouter une image.
     * le contructeur de "addPicture" contient le panel avec un JTextField pour ajouter une image, le bouton pour valider une image et la galerie d'image.
     */
    public addPicture(){

        setPreferredSize(new Dimension(350,535));
        setLayout(new BorderLayout(2,2));

        pathPicture.setPreferredSize(new Dimension(50,50));
        validationButton.setBackground(Color.white);

        addPathPanel.setLayout(new GridLayout(1,3));
        addPathPanel.setPreferredSize(new Dimension(350,30));
        addPathPanel.add(txtUser);
        addPathPanel.add(pathPicture);
        addPathPanel.add(validationButton);

        add(addPathPanel, BorderLayout.NORTH);

        // lance une exception s'il y a un problème avec la galerie d'image
        try {
            add(new allPictureWindow("/pictures.json"));
        }catch (SmartphoneException error){
            new ErrorPanel(error.getErrorCode());
        }

        // Ajouter l'actionListener au bouton qui valide l'image
        validationButton.addActionListener(new buttonAddPictureListener());

    }

    /**
     * cette méthode valide l'image. Elle contrôle que le chemin existe, ne soit pas vide,
     * que l'image n'est pas déjà présente et que l'extension soit correcte.
     * @throws SmartphoneException
     */
    public static void validationPicture(String s) throws SmartphoneException {

//        String s = pathPicture.getText();

        // vérifie si le champ est vide. Si c'est le cas on lance une exception
        if (s.length()==0){
            throw new SmartphoneException(305);
        }

        //vérifie si le chemin existe. Si ce n'est pas le cas on lance une exception.
        try{
            File pathImage = new File(ClassLoader.getSystemResource(s).toString());
        }catch (NullPointerException error){
            pathPicture.setText("");
            throw new SmartphoneException(301);
        }

        // vérifie que l'image n'est pas déjà présente dans la galerie. Si c'est le cas on lance une exception.
        for(int i=0;i<arrayListPath.size();i++){
            if(s.equals(arrayListPath.get(i))){
                GalleryWindow.addPicture();
                pathPicture.setText("");
                throw new SmartphoneException(304);
            }
        }

        // vérifie que l'extension soit correct. Si ce n'est pas les cas on lance une exception.
        String extension = s.substring(s.length() - 4);

            if (extension.equals(".jpg") || extension.equals(".png")) {
                arrayListPath.add(s);
                saveGallery("/pictures.json");
                pathPicture.setText("");
                GalleryWindow.returnToGallery();
            } else {
                pathPicture.setText("");
                throw new SmartphoneException(300);
            }

    }
    /**
     * Si pas d'exception lancée, la méthode validationPicture est appelé et le JTextField "pathPicture" est vidé.
     */
     class buttonAddPictureListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            try {
                addPicture.validationPicture(pathPicture.getText());
                pathPicture.setText("");
            } catch (SmartphoneException error) {
                new ErrorPanel(error.getErrorCode());
            }

        }
    }
}
