package Gallery;

import javax.swing.*;
import java.awt.*;

import static Gallery.allPictureWindow.saveGallery;


public class favorisPicture extends JPanel {

    private String path;
    private JButton deleteButton = new JButton("supprimer");

    /**
     * cette classe permet d'afficher une image en plein écran.
     * le constructeur de la classe "favrisPicture" contient un Jlabel avec une image.
     * @param path
     */
    public favorisPicture(String path){

        this.path=path;

        setPreferredSize(new Dimension(350,535));
        setLayout(new BorderLayout(2,2));

        // ajouter une image redimensionné à un Jlabel pour l'afficher en plein écran.
        ImageIcon image = new ImageIcon(new ImageIcon(ClassLoader.getSystemResource(path)).getImage().getScaledInstance(350,535,Image.SCALE_DEFAULT));
        JLabel img = new JLabel(image);
        add(img, BorderLayout.CENTER);

        // mise en forme et ajout du bouton "deleteButton" dans le Jpanel.
        deleteButton.setPreferredSize(new Dimension(350,50));
        deleteButton.setBackground(new Color(250,50,0));
        add(deleteButton, BorderLayout.SOUTH);

        // Ajout d'un action listener sur le bouton "deleteButton"
        deleteButton.addActionListener(new buttonDeleteFavorisPictureListener(path));

    }

    /**
     * méthode qui supprime l'image des favoris et qui affiche les favoris à jour.
     * @param path
     */
    public static void deleteFavorisPicture(String path){
        allFavorisPictures.arrayListPath.remove(path);
        saveGallery("/favorisPictures.json");
        GalleryWindow.openFavoris();
    }
}
