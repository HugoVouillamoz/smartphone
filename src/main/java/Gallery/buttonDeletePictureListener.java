package Gallery;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * la méthode "deletePicture" est appelé pour supprimer une image dans la galerie d'image.
 */

public class buttonDeletePictureListener implements ActionListener {

    public String path;

    public buttonDeletePictureListener(String path){
        this.path=path;
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        picture.deletePicture(path);
    }
}

/**
 * la méthode "deleteFavorisPicture" est appelé pour supprimer une image des favoris.
 */
class buttonDeleteFavorisPictureListener implements ActionListener{

    public String path;

    public buttonDeleteFavorisPictureListener(String path){
        this.path=path;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        favorisPicture.deleteFavorisPicture(path);
    }
}
