package Gallery;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * modification de l'action du bouton en fonction du texte du bouton
 */

public class mainButton implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {

        if(e.getSource()== GalleryWindow.addButton){

                if(GalleryWindow.addButton.getText() == "retour"){
                GalleryWindow.returnToGallery();
            }else {
                if(GalleryWindow.addButton.getText() == "ajouter"){
                    GalleryWindow.addPicture();
                }
                if(GalleryWindow.addButton.getText() == "retour à mes favoris"){
                        GalleryWindow.openFavoris();
                }
            }
        }
    }
}

/**
 *ouvre la galerie qui contient les images en favoirs
 */

class openFavoris implements ActionListener{

    @Override
    public void actionPerformed(ActionEvent e) {
        GalleryWindow.openFavoris();
    }
}
