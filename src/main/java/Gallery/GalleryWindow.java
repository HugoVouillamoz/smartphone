package Gallery;

import Errors.ErrorPanel;
import Errors.SmartphoneException;

import javax.swing.*;
import java.awt.*;

public class GalleryWindow extends JPanel {

    public static JPanel boxPanel = new JPanel();
    public static JPanel panelImage;
    public static JPanel buttonPanel = new JPanel();
    public static CardLayout cardLayout = new CardLayout();
    public static JButton addButton = new JButton("ajouter");
    public static JButton favoris = new JButton("Mes favoris");
    private GridLayout gridLayout = new GridLayout(1,2);

    /**
     * cette classe permet d'afficher le menu principal en bas de page et toutes les pages de la galerie d'image.
     * le constructeur "GalleryWindow" contient le menu principal et les différents panel de la galerie d'image.
     */
    public GalleryWindow(){

      setPreferredSize(new Dimension(350,535));
      setLayout(new BorderLayout(2,2));

      addButton.setBackground(Color.white);
      favoris.setBackground(Color.white);

      boxPanel.setPreferredSize(new Dimension(350,535));
      boxPanel.setLayout(cardLayout);

      // Ajouter des action listener pour les boutons addButton et favoris
      addButton.addActionListener(new mainButton());
      favoris.addActionListener(new openFavoris());

      // mise en place du panel contenant les boutons principaux.
       buttonPanel.setLayout(gridLayout);
       buttonPanel.add(addButton);
       buttonPanel.add(favoris);

        // lance une exception s'il y a un problème avec la galerie d'image
        try {
            panelImage = new allPictureWindow("/pictures.json");
        }catch (SmartphoneException error){
            new ErrorPanel(error.getErrorCode());
        }

        boxPanel.removeAll();
        boxPanel.add(panelImage);

        add(buttonPanel, BorderLayout.CENTER);
        add(boxPanel, BorderLayout.NORTH);

  }

    /**
     * cette méthode permet d'afficher une image en plein écran.
     * @param path
     */
  public static void openPicture(String path){

        boxPanel.remove(panelImage);
        panelImage = new picture(path);
        boxPanel.add(panelImage);
        addButton.setText("retour");
  }

    /**
     *  cette méthode permet de retourner à la galerie d'images et met à jour le texte des boutons.
     */
  public static void returnToGallery(){

        boxPanel.remove(panelImage);

      try {
          panelImage = new allPictureWindow("/pictures.json");
      }catch (SmartphoneException error){
          new ErrorPanel(error.getErrorCode());
      }

        boxPanel.add(panelImage);
        addButton.setText("ajouter");
        buttonPanel.add(addButton);
        buttonPanel.add(favoris);
  }

    /**
     * cette méthode ouvre la page qui permet d'ajouter une image et met à jour le texte du bouton en bas de page.
     */
  public static void addPicture(){

        boxPanel.remove(panelImage);
        panelImage= new addPicture();
        boxPanel.add(panelImage);
        cardLayout.next(boxPanel);
        addButton.setText("retour");
  }

    /**
     * cette méthode ouvre la page avec les images présentes dans les favoris et met à jour le texte du bouton en bas de page.
     */
  public static void openFavoris(){

      boxPanel.remove(panelImage);

      try{
          panelImage= new allFavorisPictures("/favorisPictures.json");
      }catch (SmartphoneException error){
          new ErrorPanel(error.getErrorCode());
      }

      boxPanel.add(panelImage);
      cardLayout.next(boxPanel);
      addButton.setText("retour");
      buttonPanel.remove(favoris);
  }

    /**
     * cette méthode ouvre l'image présente en favoris en plein écran et met à jour le texte du bouton en bas de page
     * @param path
     */
  public static void openFavorisPicture(String path){

      boxPanel.remove(panelImage);
      panelImage = new favorisPicture(path);
      boxPanel.add(panelImage);
      addButton.setText("retour à mes favoris");
    }
}
