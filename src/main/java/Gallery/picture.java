package Gallery;

import Contacts.allContactWindow;
import Errors.ErrorPanel;
import Errors.SmartphoneException;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static Gallery.allPictureWindow.saveGallery;


public class picture extends JPanel {

    private String path;
    private JButton deleteButton = new JButton("supprimer");
    private JButton putFavorisBtn = new JButton("mettre en favoris");
    private JPanel btnPanel = new JPanel();

    /**
     * cette classe permet d'afficher une image en plein écran
     * le constructeur "picture" contient la redimension et l'affichage d'une image ainsi que les boutons supprimer et ajouter aux favoris
     * @param path
     */

    public picture(String path){

        this.path=path;

        setPreferredSize(new Dimension(350,535));
        setLayout(new BorderLayout(2,2));

        // redimension et affichage de l'image
        ImageIcon image = new ImageIcon(new ImageIcon(ClassLoader.getSystemResource(path)).getImage().getScaledInstance(350,535,Image.SCALE_DEFAULT));
        JLabel img = new JLabel(image);
        add(img, BorderLayout.CENTER);

        // mise en forme du bouton pour supprimer une image
        deleteButton.setPreferredSize(new Dimension(350,50));
        deleteButton.setBackground(new Color(250,50,0));
        btnPanel.add(deleteButton);

        // mise en forme du bouton pour ajouter en favoris
        putFavorisBtn.setPreferredSize(new Dimension(350,50));
        putFavorisBtn.setBackground(new Color(100,100,100));
        btnPanel.add(putFavorisBtn);

        btnPanel.setLayout(new GridLayout(1,2));

        // Ajouter des action listener pour les boutons deleteButton et putFavorisBtn
        deleteButton.addActionListener(new buttonDeletePictureListener(path));
        putFavorisBtn.addActionListener(new buttonFavorisPictureListener(path));

        add(btnPanel, BorderLayout.SOUTH);

    }

    /**
     * cette méthode supprime l'image dans la galerie et si l'image est présente dans les favoris il la supprime aussi.
     * @param path
     */
    public static void deletePicture(String path){

        allPictureWindow.arrayListPath.remove(path);
        saveGallery("/pictures.json");
        allContactWindow.loadContact();

        allFavorisPictures.loadGallery("/favorisPictures.json");
        for(int i=0;i<allFavorisPictures.arrayListPath.size();i++){

            if(path.equals(allFavorisPictures.arrayListPath.get(i))){
                allFavorisPictures.arrayListPath.remove(path);
                saveGallery("/favorisPictures.json");
            }
        }
        for(int i = 0; i<allContactWindow.contacts.size(); i++){

            if(path.equals(allContactWindow.contacts.get(i).getPathContactImage())){
                allContactWindow.contacts.get(i).setPathContactImage("img/DefaultContactImage.png");
                allContactWindow.saveContact();
            }
        }

        GalleryWindow.returnToGallery();
    }

    /**
     * cette méthode ajoute une image dans les favoris. Si elle existe déjà une exception est lancée.
     * @param path
     * @throws SmartphoneException
     */
    public static void addFavorisPicture(String path) throws SmartphoneException{

           allFavorisPictures.loadGallery("/favorisPictures.json");
            for(int i=0;i<allFavorisPictures.arrayListPath.size();i++){
                if(path.equals(allFavorisPictures.arrayListPath.get(i))){
                    GalleryWindow.returnToGallery();
                    throw new SmartphoneException(303);
                }
            }

            allFavorisPictures.arrayListPath.add(path);
            saveGallery("/favorisPictures.json");
            GalleryWindow.openFavoris();

    }
}


class buttonFavorisPictureListener implements ActionListener {

    public String path;

    public buttonFavorisPictureListener(String path){
        this.path=path;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            picture.addFavorisPicture(path);
        }catch (SmartphoneException error){
            new ErrorPanel(error.getErrorCode());
        }
    }
}
