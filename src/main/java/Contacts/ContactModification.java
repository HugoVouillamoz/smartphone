package Contacts;

import Errors.ErrorPanel;
import Errors.SmartphoneException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Panel utilisé pour afficher la page de modification d'un contact
 */
public class ContactModification extends JPanel {

    public static  String pathToContactImage;
    public static String newPathToContactImage = null ;
    public static JPanel underPanel = new JPanel();
    public static CardLayout myCardLayout = new CardLayout();

    private int contactPosition;
    private  JTextField firstNameTextField = new JTextField("first");
    private  JTextField lastNameTextField = new JTextField("last");
    private  JTextField telNumberTextField = new JTextField("telNumber");
    private JLabel prenom = new JLabel("Prénom :");
    private JLabel nom = new JLabel("Nom :");
    private JLabel telephone = new JLabel("Numéro de téléphone :");
    private Font myFont = new Font("Arial", Font.ITALIC, 15);
    private JButton pictureModification = new JButton("Changer photo du contact");
    private  JPanel contactPicture = new JPanel();
    private  JPanel contactInfo = new JPanel();
    private GridLayout gridLayout = new GridLayout(2,1,0,0);
    private JButton cancelButton = new JButton("Cancel");
    private JButton validationButton = new JButton("Validation");
    private Contact contact;

    /**
     *
     * @param contact le contact qui doit être modifié
     * @param positionContact son emplacement dans l'arrayList
     * Le constructeur va créer une variable temporaire de type string afin de stocker le chemin d'une nouvelle image de profile
     * et de faire la modification seuelement une fois que le bouton valider sera préssé
     * La page contient 3 JTextField dans lequel l'utilisateur peut entrer les valeurs souhaitées
     * Ces valeurs seront alors tester par les méthodes de la classe contact afin d'en vérifier la validité
     */
    public ContactModification(Contact contact, int positionContact){

        if(newPathToContactImage != null){                                                                              // condition pour voir si on crée cette page en venant de la page infoContact ou si on la crée en venant de la validation de la photo
            pathToContactImage = newPathToContactImage ;                                                                // si le nouveau path n'est pas null on affiche alors cette image la (mais le JSon n'est modifié qu'une fois le bouton valider cliqué)
        }else {
            pathToContactImage = contact.getPathContactImage();
        }

        this.contact = contact;
        this.contactPosition = positionContact;

        ContactWindow.cleanCardLayout(ContactWindow.cardLayoutPanel);
        setOpaque(false);
        setLayout(myCardLayout);

        firstNameTextField.setText(contact.getFirstName());
        lastNameTextField.setText(contact.getLastName());
        telNumberTextField.setText(contact.getTelNumber());

        underPanel.removeAll();
        underPanel.setLayout(gridLayout);
        underPanel.setOpaque(false);

        pictureModification.setBorderPainted(false);
        pictureModification.addActionListener(new ModificationPhotoListener());

        contactPicture.setLayout(new BorderLayout(1,1));
        contactPicture.add(new PhotoContact(pathToContactImage), BorderLayout.CENTER);
        contactPicture.add(pictureModification,BorderLayout.SOUTH);
        contactPicture.setOpaque(false);

        prenom.setFont(myFont);
        nom.setFont(myFont);
        telephone.setFont(myFont);

        contactInfo.setOpaque(false);
        contactInfo.setLayout(new GridLayout(8,1,5,5));
        contactInfo.add(prenom);
        contactInfo.add(firstNameTextField);
        contactInfo.add(nom);
        contactInfo.add(lastNameTextField);
        contactInfo.add(telephone);
        contactInfo.add(telNumberTextField);

        cancelButton.addActionListener(new ContactModificationtListener());
        cancelButton.setBackground(new Color(222,96,96));
        validationButton.addActionListener(new ContactModificationtListener());
        validationButton.setBackground(new Color(124,238,120));

        contactInfo.add(validationButton);
        contactInfo.add(cancelButton);

        underPanel.add(contactPicture);
        underPanel.add(contactInfo);

        add(underPanel);
    }

    public static void setNewPathToContactImage(String path){
        newPathToContactImage = path;
    }


    /**
     * Permet de modifier le panel afficher en le remplacent avec un nouveau (ex: panel avec toutes les images)
     * @param newPanel le nouveau panel à afficher
     */
    public void changePanelPicture(JPanel newPanel) {
        remove(underPanel);
        underPanel = newPanel;
        add(underPanel);
        myCardLayout.next(this);
    }

    /**
     * création des listeners pour les boutons valider et annuler
     * Le bouton valider va alors appliqué les différentes modifications qui ont eu lieu via la méthode addOrModifyContact qui
     * va ensuite chargé un panel InformationContact contenant les nouvelles informations
     * Si le bouton choisi et celui de retour les modifications sont annulées et un panel information contact sera chargé avec
     * les anciennes informations
     */

    public class ContactModificationtListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e){

            newPathToContactImage = null;
           if(e.getSource() == validationButton){

               try {
                   allContactWindow.addOrModifyContact(new Contact(firstNameTextField.getText(), lastNameTextField.getText(), telNumberTextField.getText(), pathToContactImage), contactPosition);
               }catch(SmartphoneException error){
                   new ErrorPanel(error.getErrorCode());
               }

           }else if(e.getSource() == cancelButton){
               ContactWindow.switchToContactInfo(contact, contactPosition);
           }
        }
    }

    /**
     * modification du bouton situé en bas de l'écran qui permet maintenant de revenir à la modification du contact quand on se
     * trouve sur la page du choix de la nouvelle image contact
     */
    public class ModificationPhotoListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {


            ContactWindow.buttonAddContact.setText("Retour modification");
            changePanelPicture(new ChangeContactPicture(contact, contactPosition));
        }
    }
}
