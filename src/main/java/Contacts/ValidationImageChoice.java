package Contacts;

import Errors.SmartphoneException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Panel affichant la nouvelle photo choisi par l'utilisateur
 */
public class ValidationImageChoice extends JPanel {

    private JPanel myPanel = new JPanel();
    private String path;
    private JButton validerButton = new JButton("valider");
    private JButton backButton = new JButton("Retour");


    /**
     * Affichage de l'image en grand avec les boutons valider et retour à choix
     * Le bouton valider permet de revenir à la modificationd du contact avec la nouvelle image choisi
     * le bouton retour permet de revenir au panel avec toutes les images à choix
     * @param path chemin de l'image choisi pour validation
     */
    public ValidationImageChoice(String path) {

        this.path = path;
        setOpaque(false);
        setPreferredSize(new Dimension(350,400));
        ImageIcon image = new ImageIcon(new ImageIcon(ClassLoader.getSystemResource(path)).getImage().getScaledInstance(350,450,Image.SCALE_DEFAULT));
        JLabel img = new JLabel(image);
        myPanel.setLayout(new GridLayout(6,1));
        myPanel.setOpaque(false);
        myPanel.add(validerButton);
        myPanel.add(backButton);
        add(img,BorderLayout.CENTER);
        add(myPanel,BorderLayout.SOUTH);
        backButton.setBackground(new Color(222,96,96));
        backButton.setPreferredSize(new Dimension(200, 25));
        validerButton.setBackground(new Color(124,238,120));
        validerButton.setPreferredSize(new Dimension(200,25));
        validerButton.addActionListener(new ChoosePicture());
        backButton.addActionListener(new ChoosePicture());

    }

    /** listener permettant de revenir en arrière ou de valider le choix de la nouvelle image
     */
    public class ChoosePicture implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if(e.getSource() == validerButton ) {
                ContactModification.setNewPathToContactImage(path);
                ContactWindow.switchToModifyContact(ChangeContactPicture.modificationContact, ChangeContactPicture.positionContactEnModification);
            }
            if(e.getSource() == backButton){
                try {
                    ChangeContactPicture.switchBack(new allPictures());
                }catch (SmartphoneException error){

                }
            }
        }
    }
}
