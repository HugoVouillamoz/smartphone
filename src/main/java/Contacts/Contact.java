package Contacts;

import Errors.SmartphoneException;

/**
 * Class permettant de créer des objets de type contact
 */
public class Contact {

    private String firstName;
    private String lastName;
    private String telNumber;
    private String pathContactImage;

    /**
     * Constructeur par défauts utiliser pour créer des nouveaux contact sans informations
     */
    public Contact(){
        firstName = " ";
        lastName = " ";
        telNumber = " ";
        pathContactImage = "img/DefaultContactImage.png";
    }

    /**
     * Constructeur avec arguments
     * @param firstName
     * @param lastName
     * @param telNumber
     * @param pathContactImage
     * @throws SmartphoneException
     * Le constrcuteur va ensuite faire appel au différents Setter pour modifier les valeurs du nouveau contact
     * Si l'un des setter échoue une Smartphone exception sera alors lancé et attraper par le constructeur
     */
    public Contact(String firstName, String lastName, String telNumber, String pathContactImage) throws SmartphoneException {

        try {
            setFirstName(firstName);
            setLastName(lastName);
            setTelNumber(telNumber);
            setPathContactImage(pathContactImage);
        }catch(SmartphoneException error){
          throw new SmartphoneException(error.getErrorCode());
        }
    }

    public String getFirstName(){
        return firstName;
    }

    public String getLastName(){
        return lastName;
    }

    public String getTelNumber(){
        return telNumber;
    }

    public String getPathContactImage(){
        return pathContactImage;
    }

    public String FullName(){                                                                                           // méthode qui crée le nom complet (prénom + nom) afin de les triers par ordre alphabétique correctement
        return lastName + " " + firstName;
    }

    /**
     * Setter permettant de vérifier que le prénom n'est pas vide
     */
    public void setFirstName(String firstName) throws SmartphoneException{
        if(controleEntryValidity(firstName)){
            this.firstName = firstName.trim();
        }else{
            throw new SmartphoneException(100);
        }

    }

    /**
     * Setter permettant de vérifier que le nom n'est pas vide
     */
    public void setLastName(String lastName) throws SmartphoneException{
        if(controleEntryValidity(lastName)){
            this.lastName = lastName.trim();
        } else{
            throw new SmartphoneException(101);
        }
    }

    /**
     * Setter permettant de vérifier que le numéro n'est pas vite et qu'il commence bien par 0
     * et que les charactères sont bien des chiffres
     */
    public void setTelNumber(String telNumber)throws SmartphoneException{

        if(controleEntryValidity(telNumber)) {

                if (telNumberValidity(telNumber)) {
                    this.telNumber = telNumber;
                } else {
                    throw new SmartphoneException(103);
                }
        }else{
            throw new SmartphoneException(102);
        }
    }

    public void setPathContactImage(String pathContactImage){
        this.pathContactImage = pathContactImage;
    }


    /**
     * Méthode utilisée dans les setter qui permet de vérifier si une string contient des charactères ou non
     */
    public static boolean controleEntryValidity(String valeur){

        valeur = valeur.replaceAll(" ", "");
        if(!valeur.isEmpty()){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Méthode utilisée par le setter du numéro de téléphone afin de vérifier la validité de ce dernier
     */
    public static boolean telNumberValidity(String telnumber){
        telnumber = telnumber.replaceAll(" ", "");

        if(telnumber.charAt(0) != '0'){
            return false;
        }
        for(int i = 1; i < telnumber.length(); i++){
            if(telnumber.charAt(i) < 48 || telnumber.charAt(i) > 57){
                return false;
            }
        }
        return true;
    }

}
