package Contacts;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.RoundRectangle2D;

/**
 * Class affichant l'image du contact pour les panel informations et modification contact
 */
public class PhotoContact extends JPanel {

    private String pathToImage;
    private JButton imageButton;
    private RoundRectangle2D.Double SHAPE = new RoundRectangle2D.Double(0,0,300,150,90,90);


    /**
     * Création d'un panel dans lequel on affiche seulement l'image du contact
     * @param pathToImage chemin de l'image du contact voulu
     */
    public PhotoContact(String pathToImage){

        this.pathToImage = pathToImage;

        setLayout(new CardLayout());
        setPreferredSize(new Dimension(350,200));
        setOpaque(false);

        imageButton = new JButton(new ImageIcon(new ImageIcon(ClassLoader.getSystemResource(pathToImage)).getImage().getScaledInstance(250,250,Image.SCALE_DEFAULT)));
        imageButton.setBorderPainted(false);
        imageButton.setContentAreaFilled(false);

        add(imageButton);
    }

}
