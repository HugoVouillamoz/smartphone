package Contacts;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Class principal de l'application contact dans laquelle on utilise un cardLayout afin de changer le panel qui est affiché
 */

public class ContactWindow extends JPanel{


    public static CardLayout cardLayout = new CardLayout();
    public static JButton buttonAddContact = new JButton();
    public static JPanel cardLayoutPanel = new JPanel();
    public static JPanel contactContent;

    private JPanel contentButton = new JPanel();
    private Dimension SIZE = new Dimension(350,535);
    private Image background = new ImageIcon(ClassLoader.getSystemResource("img/ContactBackground.png")).getImage();

    /**
     * création de la page de base de l'application
     * Un panel contenant tous les contacts (class allContactWindow) est créé et insérer dans le panel contenant le cardlayout
     * Un bouton servant à créer des nouveux contacts est également ajouter au panel
     */
    public ContactWindow(){

        setPreferredSize(SIZE);
        setLayout(new BorderLayout(2,15));
        cleanCardLayout(cardLayoutPanel);

        cardLayoutPanel.setOpaque(false);
        cardLayoutPanel.setPreferredSize(SIZE);
        cardLayoutPanel.setLayout(cardLayout);

        buttonAddContact.setText("ajouter");
        buttonAddContact.setPreferredSize(new Dimension(200,50));
        buttonAddContact.addActionListener(new addBackButton());

        contentButton.setOpaque(false);
        contentButton.add(buttonAddContact, BorderLayout.SOUTH);
        contactContent = new allContactWindow();
        contactContent.setOpaque(false);

        cardLayoutPanel.add(contactContent);

        add(cardLayoutPanel, BorderLayout.NORTH);
        add(contentButton, BorderLayout.CENTER);

    }

    /**
     * Méthode servant à effacer tout le contenu du Jpanel avec le card layout
     * Cette méthode est utilisée quand on retourne au menu principal afin de vider le panel et de relancer
     * l'application au panel principal la fois suivante
     */
    public static void cleanCardLayout(JPanel myPanel){
        myPanel.removeAll();
    }

    /**
     * Méthode utilisée pour afficher une image en fond d'écran derrière l'application contact
     */
    public void paintComponent( Graphics g){
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.drawImage(background,0,0,350,750, null);
    }

    /**
     * Méthode utiliser pour changer le panel principal en panel contenant les informations d'un contact
     * @param contact contact à afficher
     * @param positionContact position du contact à afficher
     */
    public static void switchToContactInfo(Contact contact, int positionContact){

            cardLayoutPanel.remove(contactContent);
            contactContent = new InfoContactWindow(contact, positionContact);
            contactContent.setOpaque(false);
            cardLayoutPanel.add(contactContent);
            cardLayout.next(cardLayoutPanel);
            buttonAddContact.setText("retour");

    }

    /**
     * Modification du panel actuel en panel de modification du contact
     * @param contact contact à modifier
     * @param positionContact position du contact
     */
    public static void switchToModifyContact(Contact contact, int positionContact){

        cardLayoutPanel.remove(contactContent);
        contactContent = new ContactModification(contact, positionContact);
        contactContent.setOpaque(false);
        cardLayoutPanel.add(contactContent);
        cardLayout.next(cardLayoutPanel);
        buttonAddContact.setText("retour");
    }

    /**
     * méthode permettant de revenir au panel contenant tous les contacts
     */
    public static void switchToAllContact(){

        cardLayoutPanel.remove(contactContent);
        contactContent = new allContactWindow();
        contactContent.setOpaque(false);
        cardLayoutPanel.add(contactContent);
        buttonAddContact.setText("ajouter");
    }

    /**
     * création de listener pour le bouton se trouvant en bas de l'écran
     * le listener aura une action différente suivant le text attribué au bouton (retour / ajouter/ retour modification
     */
    public  class addBackButton implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e){

             if(e.getSource() == buttonAddContact){
                 if(buttonAddContact.getText() == "retour") {
                     switchToAllContact();
                 }else if (buttonAddContact.getText() == "ajouter"){
                   switchToModifyContact(new Contact(), allContactWindow.contacts.size()+1);
                 }else if(buttonAddContact.getText() == "Retour modification"){
                        switchToModifyContact(ChangeContactPicture.modificationContact, ChangeContactPicture.positionContactEnModification);
                 }
             }
        }
    }

}
