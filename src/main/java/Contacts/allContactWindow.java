package Contacts;

import Errors.SmartphoneException;
import SaveAndLoad.JSONStorage;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;


public class allContactWindow extends JPanel{

    public static ArrayList<Contact> contacts = new ArrayList<Contact>();
    public static JSONStorage jsonStorage = new JSONStorage();
    public static String path;

    private JButton contactButtons[] = new JButton[1] ;
    private Dimension SIZE = new Dimension(350,535);
    private JPanel allContactPanel = new JPanel();
    private JScrollPane jScrollPane = new JScrollPane();

    /**
     * Constructeur qui va générer un bouton pour chacun des contacts présents dans le fichier JSON
     * Le constructeur va tout d'abord charger les contacts présents dans le fichier JSON
     * Ensuite il va les trier par ordre alphabétique grâce au comparator que nous avons créé plus bas
     * Chaque bouton va contenir le nom et prénom du contact dans un text field et le numéro sera affiché dans le texte du bouton lui même
     * Si le nombre de contacts et inférieur à 7 le GridLayout va quand même séparer l'écran en 7 part égal afin de ne pas modifier la taille d'un contact
     * Si il y a 0 contact le constructeur affiche un seul bouton non cliquable qui annonce qu'il n'y a aucun contact
     */

    public allContactWindow(){

        loadContact();

        contacts.sort(new ContactNameComparator());
        JButton buttonContacts[] = new JButton[contacts.size()];

        for(int i = 0; i < contacts.size(); i++){

            JButton buttonName = new JButton( contacts.get(i).getTelNumber());
            JLabel myLabel = new JLabel(contacts.get(i).getFirstName() + " " +  contacts.get(i).getLastName());
            buttonName.setBorderPainted(false);
            buttonName.add(myLabel);
            buttonContacts[i] = buttonName;
            buttonContacts[i].addActionListener(new contactListener());
            allContactPanel.add(buttonContacts[i]);
        }

        setOpaque(false);
        setLayout(new CardLayout());
        setPreferredSize(new Dimension(350, 535));

        allContactPanel.setOpaque(false);

        if(contacts.size() > 7) {
            allContactPanel.setPreferredSize(new Dimension(400, contacts.size() * 75));
            allContactPanel.setLayout(new GridLayout(contacts.size(), 1, 5, 5));
        }else{
            allContactPanel.setPreferredSize(new Dimension(400, 525));
            allContactPanel.setLayout(new GridLayout(7,1,5,5));
            if(contacts.size() == 0){
                JButton button = new JButton("Aucun contact");
                button.setContentAreaFilled(false);
                button.setBorderPainted(false);
                allContactPanel.add(button);
            }
        }

        jScrollPane.getVerticalScrollBar().setUnitIncrement(20);
        jScrollPane.setWheelScrollingEnabled(true);
        jScrollPane.setViewportView(this);
        jScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        jScrollPane.getViewport().add(allContactPanel);
        jScrollPane.setOpaque(false);

        add(jScrollPane);
    }

    /**
     *
     * @param contactPosition grâce à la position du contact la méthode sait quel index de l'arrayList il faut supprimer
     */

    public static void removeContact(int contactPosition){
        contacts.remove(contactPosition);
        saveContact();
        ContactWindow.switchToAllContact();
    }

    /**
     *
     * @param contact  la méthode prend en paramètre un contact, soit le contact à modifier soit un contact vide afin de créer le nouveau
     * @param contactPosition la position du contact est stocké dans le cas d'une modification, si c'est un nouveau contact la position est égal au nombre de contact +1
     * @throws SmartphoneException des erreurs peuvent être lancés si les informations du contact sont fauses
     *
     * la méthode va vérifier si la position du contact est supérieur au nombre de contact si c'est le cas elle va simplement l'ajouter à l'arrayList
     * si la position du contact pointe sur un contact excistant le programme va alors remplacer le contact par celui que nous venons de modifiers
     */

    public static void addOrModifyContact(Contact contact, int contactPosition) throws SmartphoneException {

        for(int i = 0; i < contacts.size(); i++){
            if(i != contactPosition) {
                if (contacts.get(i).getTelNumber().equals(contact.getTelNumber())) {
                    throw new SmartphoneException(104);
                }
            }
        }
        if(contactPosition > contacts.size()){
            contacts.add(contact);
        }else {
            contacts.set(contactPosition, contact);
        }
        saveContact();
        ContactWindow.switchToContactInfo(contact,contactPosition);
    }

    /**
     * Méthode servant à écrire les modifications des contacts dans le fichier JSON
     * On utilise ici la variable système créée plus tôt afin de retrouver le chemin du fichier
     */
    public static void saveContact(){
        jsonStorage.write(new File(System.getenv("Smartphone") + "\\contacts.json"), contacts);
    }

    /**
     * Méthode servant à charger les contacts présents dans le JSON au lancement de l'application contact
     */
    public static void loadContact(){
        contacts = jsonStorage.read(new File(System.getenv("Smartphone") + "\\contacts.json") , contacts );
    }

    /**
     * Class servant à créer un comparateur que l'on va ensuite utiliser afin de classer les contacts par ordre alphabétique
     */
    public class ContactNameComparator implements Comparator<Contact>{

        public int compare(Contact contact1, Contact contact2){
            return contact1.FullName().toLowerCase().compareTo(contact2.FullName().toLowerCase());
        }
    }

    /**
     * Class créant les listeners pour les boutons des contacts
     * La class réconnait qu'elle bouton a été présser grâce au numéro de téléphone qu'il contient
     * la boucle for va ensuite trouver à qu'elle indice de contact correspond ce numéro
     */
    public  class contactListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e){

            String infoContact;
            JButton clickedButton = (JButton) e.getSource();

            for(int i = 0; i < contacts.size(); i++){
                infoContact = contacts.get(i).getTelNumber();
                if(clickedButton.getText().equals(infoContact)){
                    ContactWindow.switchToContactInfo(contacts.get(i), i);
                }
            }

        }
    }

}
