package Contacts;

import Smartphone.SmartphoneShape;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

/**
 * Class utlilisée pour créer un panel affichant les informations d'un contact
 */


public class InfoContactWindow extends JPanel {

    private JButton modifyContact = new JButton("modify");
    private JButton deleteContact = new JButton("Delete");
    private  JLabel firstNameLabel = new JLabel("first");
    private  JLabel lastNameLabel = new JLabel("last");
    private  JLabel telNumberLabel = new JLabel("telNumber");
    private JLabel prenom = new JLabel("Prénom :");
    private JLabel nom = new JLabel("Nom :");
    private JLabel telephone = new JLabel("Numéro de téléphone :");
    private Font myFont = new Font("Arial", Font.ITALIC, 15);
    private  String pathToContactImage;
    private  JPanel photoContact = new JPanel();
    private  JPanel infoContact = new JPanel();
    private int  positionContact;
    private GridLayout gridLayout = new GridLayout(2,1,5,5);

    private GridBagLayout gridBagLayout = new GridBagLayout()   ;
    private GridBagConstraints gbc = new GridBagConstraints();

    /**
     * Création du panel avec les informations du contacts
     * @param contact contact dont on veux les informations
     * @param positionContact position du contact affiché
     * Les différents informations du contacts sont affichées dans des Jlabels afin que l'utilisateur ne puisse pas les
     * modifiés depuis ce panel
     */
    public InfoContactWindow(Contact contact, int positionContact){

        this.positionContact = positionContact;

        setOpaque(false);
        setLayout(gridLayout);
        pathToContactImage = contact.getPathContactImage();
        firstNameLabel.setText(contact.getFirstName());
        lastNameLabel.setText(contact.getLastName());
        telNumberLabel.setText(contact.getTelNumber());


        photoContact.add(new PhotoContact(contact.getPathContactImage()));
        photoContact.setOpaque(false);

        infoContact.setOpaque(false);
        infoContact.setLayout(new GridLayout(8,1,5,5));
        prenom.setFont(myFont);
        nom.setFont(myFont);
        telephone.setFont(myFont);
        infoContact.add(prenom);
        infoContact.add(firstNameLabel);
        infoContact.add(nom);
        infoContact.add(lastNameLabel);
        infoContact.add(telephone);
        infoContact.add(telNumberLabel);

        modifyContact.addActionListener(new InfoContactListener());
        modifyContact.setBackground(new Color(124,238,120));
        deleteContact.addActionListener(new InfoContactListener());
        deleteContact.setBackground(new Color(222,96,96));


        infoContact.add(modifyContact);
        infoContact.add(deleteContact);

        add(photoContact);
        add(infoContact);
    }

    /**
     * Création d'un listener permettant d'attribuer une action au bouton modification (création d'un panel modification)
     * et au bouton servant à supprimer un contact
     */
    public class InfoContactListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e){

            if(e.getSource() == modifyContact){
                ContactModification.newPathToContactImage = null;
                ContactWindow.switchToModifyContact(allContactWindow.contacts.get(positionContact),positionContact);
            }else if(e.getSource() == deleteContact){
                allContactWindow.removeContact(positionContact);
            }
        }
    }
}
