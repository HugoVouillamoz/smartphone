package Contacts;

import Errors.SmartphoneException;
import Gallery.allPictureWindow;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Class étendu de allPictureWindwo dans la gallery
 * Permet de créer un panel contenant toutes les images de la gallery
 */

public class allPictures extends allPictureWindow {

    public allPictures() throws SmartphoneException {
        super("/pictures.json");
        setOpaque(false);
    }

    /**
     * Override de la méthode setListener présente dans la class allPictureWindow afin d'utiliser un listener propre
     * au chois de l'image du contact
     */
    @Override
    public void setListener(){
        for(int i = 0; i < pictureBtnArray.length; i++){
            pictureBtnArray[i].addActionListener(new ViewPicture(arrayListPath.get(i)));
        }
    }

    /**
     * Création d'un listener qui nous permet d'afficher l'image choisi pour un contact en grand
     */

    public static class ViewPicture implements ActionListener {
        public String path;

        public ViewPicture(String path){
            this.path=path;
        }
        @Override
        public void actionPerformed(ActionEvent e) {
            ChangeContactPicture.openPicture(path);
        }
    }
}
