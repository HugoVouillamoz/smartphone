package Contacts;


import Errors.SmartphoneException;

import javax.swing.*;
import java.awt.*;

public class ChangeContactPicture extends JPanel {


    public static int positionContactEnModification;

    public static JPanel contentPanel = new JPanel();
    public static JPanel allPicturePanel = new JPanel();
    public static CardLayout myCardLayout = new CardLayout();

    public static Contact modificationContact;

    /**
     *
     * @param contact contact pour lequel on veut changer l'image du profil
     * @param contactPosition position du contact afin de récupérer la photo actuel
     * Cette class sert à créer un panel dans lequel on va changer son contenu entre le panel contenant toutes les images
     * et le panel contenant une image en grand et permettant de valider le choix
     */

    public ChangeContactPicture(Contact contact, int contactPosition){

        modificationContact = contact;
        positionContactEnModification = contactPosition;
        contentPanel.setLayout(myCardLayout);
        contentPanel.removeAll();
        try{
            contentPanel.add(new allPictures());
        }catch (SmartphoneException error){

        }
        add(contentPanel);
    }

    /**
     *
     * @param path chemin de l'image choisi
     * La méthode permet de passer du panel contenant toutes les images a un panel contenant l'image choisi en grand
     */
    public static void openPicture(String path){

        contentPanel.remove(allPicturePanel);
        allPicturePanel = new ValidationImageChoice(path);
        allPicturePanel.setOpaque(false);
        contentPanel.add(allPicturePanel);
        myCardLayout.next(contentPanel);

    }

    /**
     *
     * @param newPanel nouveau panel à afficher à l'écran
     * Méthode servant à revenir au panel avec toutes les images
     */

    public static void switchBack(JPanel newPanel){

        contentPanel.remove(allPicturePanel);
        allPicturePanel = newPanel;
        contentPanel.add(allPicturePanel);
        myCardLayout.next(contentPanel);
    }


}
