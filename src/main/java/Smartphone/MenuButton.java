package Smartphone;

import MenuPrincipal.WindowMenu;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.RoundRectangle2D;
import java.util.zip.GZIPInputStream;

/**
 * Class servant à créer le JPanel qui contient le Boutton retour en bas de l'écran
 */
public class MenuButton extends JPanel {

        private JButton backButton = new JButton();

    /**
     * Création du panel et du bouton retour
     * Le bouton est rendu invisible et on dessine simplement un cercle noir
     */
    public MenuButton(){

        setBackground(new Color(169,169,169));
        setPreferredSize(new Dimension(350, 75));
        setLayout(new BorderLayout());

        GridBagConstraints constraints = new GridBagConstraints();
        GridBagLayout layout = new GridBagLayout() ;
        setLayout(layout);

        constraints.gridx = 0;
        constraints.gridy = 0;
        backButton.setPreferredSize(new Dimension(50,50));
        backButton.addActionListener(new BackButtonListener() );

        backButton.setBounds(0,0,70,70);
        backButton.setBorder(new RoundedBorder(70));
        backButton.setContentAreaFilled(false);
        backButton.setForeground(Color.black);

        add(backButton, constraints);

    }

    /**
     * Ajout du listener du bouton retour qui permet de changer le contenu du panel principal en y mettant le menu
     * principal peu importe oÙ l'on se trouve
     */

    public class BackButtonListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            SmartphoneShape.changeApplication(new WindowMenu());
        }
    }

    /**
     * Classe servant à dessiner un cercle pour le bouton
     */

    private static class RoundedBorder implements Border {

        private int radius;


        RoundedBorder(int radius) {
            this.radius = radius;
        }


        public Insets getBorderInsets(Component c) {
            return new Insets(this.radius+1, this.radius+1, this.radius+2, this.radius);
        }


        public boolean isBorderOpaque() {
            return true;
        }


        public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
            g.drawRoundRect(x, y, width-1, height-1, radius, radius);
        }
    }

}
