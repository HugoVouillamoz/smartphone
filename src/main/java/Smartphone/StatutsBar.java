package Smartphone;

import javax.swing.*;
import javax.swing.text.Style;
import java.awt.*;
import java.awt.image.Kernel;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Class servant à afficher l'heure et la date dans un panel au sommet de l'écran
 */

public class StatutsBar extends JPanel {

    private JLabel time = new JLabel();
    private JLabel date = new JLabel();
    Thread monThread = new Thread();

    /**
     * Création du panel contenant l'heure du système et la date du système
     */

    public StatutsBar(){

        Calendar calendar = GregorianCalendar.getInstance() ;
        time.setText(calendar.get(Calendar.HOUR_OF_DAY) + " : " + calendar.get(Calendar.MINUTE));
        date.setText(calendar.get(Calendar.DAY_OF_MONTH) + "/" + (calendar.get(Calendar.MONTH) +1));
        add(time);
        add(date);
        setBackground(new Color(169,169,169));
        setPreferredSize(new Dimension(400,35));

    }
}
