package Smartphone;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.RoundRectangle2D;

/**
 * JFrame principale qui va contenir un panel central qui va changer en fonction de ce qu'on veut afficher
 */

public class SmartphoneShape extends JFrame {

    private Dimension SIZE = new Dimension(350,750);                                           //Taille de la JFrame qui contiendra tous les panels du projets
    private RoundRectangle2D.Double SHAPE = new RoundRectangle2D.Double(0,0,350,750,90,90);       //Création d'un roundRectangle afin d'arrondir les angles du smartphone

    private StatutsBar statutsBar = new StatutsBar();                                                                   // instance de la classe StatutsBar qui permet d'afficher le panel supérieur contenant l'heure/date ect...
    private MenuButton menuButton = new MenuButton();                                                                   // instance de la classe menu bouton qui permet d'afficher le panel inférieur contenant le bouton retour
    private static JPanel applicationContent = new JPanel();                                                            // panel inférieur qui contient une des 4 pages principale (menu ou une app)
    private static JPanel application;                                                                                  // panel qui deviendra une des 4 applications en fonctions des boutons cliqués

    private static CardLayout layout = new CardLayout();

    /**
     * Création du panel en y mettant la SHAPE du smartphone ce qui lui donne des bords arrondis
     * @param application c'est le panel qu'on va afficher dans le smartphone
     */
    public SmartphoneShape(JPanel application){

                                                                                                                        //création de la JFrame selon les besoins, on va ensuite garder cette même JFrame tout le long du programme en modfiant simplement le panel central
        this.application = application;
        setPreferredSize(SIZE);
        setUndecorated(true);
        setShape(SHAPE);
        setVisible(true);
                                                                                                                        //le panel application est le panel principal du smartphone c'est celui qui accuillera soit l'écran d'accueil soit une des 3 applications
        applicationContent.setLayout(layout);
        applicationContent.add(application);
                                                                                                                        //mise en place de la status bar (en haut) et du boutton de retour au menu (en bas)
        setLayout(new BorderLayout(1,1));
        add(statutsBar, BorderLayout.NORTH);
        add(applicationContent, BorderLayout.CENTER);
        add(menuButton, BorderLayout.SOUTH);
        pack();
        setLocationRelativeTo(null);
    }

    /**
     * Perme de passer un panel dans la signature de la méthode et ainsi on affiche ce nouveau panel au centre du smartphone
     * @param newPage panel à afficher
     */

    public static void changeApplication(JPanel newPage){

        applicationContent.remove(application);
        application = newPage;                                                                                          //methode qui enleve le panel présent sur l'écran et qui ajoute la nouvelle page à afficher (une application ou le menu)
        applicationContent.add(application);
        layout.next(applicationContent);                                                                                //le next() sert à passer au panel suivant charger dans le layout c'est à dire celui qui vient d'être ajouté



    }


}

