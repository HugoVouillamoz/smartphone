package SaveAndLoad;

import Errors.SmartphoneException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class JSONStorageGallery {

    public static ArrayList<String> read(File source, ArrayList<String> path)  {

        ObjectMapper mapper = new ObjectMapper();
        String [] pathArray;

        try{
            pathArray = mapper.readValue(source, String[].class);
            path = new ArrayList<>(Arrays.asList(pathArray));
        }catch ( IOException e){
            System.out.println("Le fichier JSON est soit vide soit introuvable");
        }

        return path;
    }


    public static void write(File destination, ArrayList<String> path){

        ObjectMapper mapper = new ObjectMapper();

        try{
            mapper.writeValue(destination, path);
        }catch (IOException e){
            System.out.println("Erreur lors de l'écriture du fichier Json");
        }
    }




}
