package SaveAndLoad;

import Contacts.Contact;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Class utilisée pour écrire et lire le fichiers JSON en rapport avec les contacts
 */

public class JSONStorage{

    /**
     * Méthode qui va lire le contenu du fichier JSON
     * @param source la source ou se situe le fichier JSON
     * @param contacts L'array list qui va ensuite être retournée une fois remplie
     */
    public ArrayList<Contact> read(File source, ArrayList<Contact> contacts) {

        ObjectMapper mapper = new ObjectMapper();
        Contact [] contactArray;
        try{
            contactArray = mapper.readValue(source, Contact[].class);
            contacts = new ArrayList<>(Arrays.asList(contactArray));
        }catch ( IOException e){
            e.printStackTrace();
            System.out.println("Error while reading JSON contact");
        }


        return contacts;
    }

    /**
     * Méthode qui va venir écrire les modifications ou l'ajout des contacts dans le fichier JSON
     * @param destination chemin d'accès jusqu'au fichier JSON
     * @param contacts arrayList contenant tous les contacts
     */
    public void write(File destination, ArrayList<Contact> contacts) {

        ObjectMapper mapper = new ObjectMapper();

        try{

            mapper.writeValue(destination, contacts);
        }catch (IOException e){

            System.out.println("Crash while writting");

        }
    }
}
