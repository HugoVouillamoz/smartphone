import MenuPrincipal.*;
import Smartphone.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class Smartphone_Main {

    /**
     * Si c'est la première fois que vous excécuter le code il va falloir créer une variable d'environement nommée Smartphone qui va permettre de trouver le répertoire contenant les JSON
     * Vous pouvez la créer facilement via la commande suivante :
     * SETX Smartphone PATH  (le SETX au lieu du set permet de créer une variable qui reste d'une session à l'autre)
     * Ne pas oublier de redemarrer IntelliJ pour mettre à jours les variables d'environement
     * On va ensuite pouvoir utiliser cette variable afin de retrouver le json à chaque nouvelle ouverture du smartphone
     */

    public static void main(String[] args) throws IOException {

        System.out.println(System.getenv("Smartphone"));

        File contactList = new File(System.getenv("Smartphone") + "/contacts.json");
        File picturesList = new File(System.getenv("Smartphone") + "/pictures.json");
        File favorisPictures = new File(System.getenv("Smartphone") + "/favorisPictures.json");



        if (!contactList.exists()) {
            contactList.createNewFile();

            String str1 = "[]";

            byte[] strToBytes = str1.getBytes();

            Files.write(Path.of(String.valueOf(contactList)), strToBytes);
            System.out.println("Création du fichier contact");
        }
        if (!picturesList.exists()) {
            picturesList.createNewFile();

            String str1 = "[]";

            byte[] strToBytes = str1.getBytes();

            Files.write(Path.of(String.valueOf(picturesList)), strToBytes);

            System.out.println("Création du fichier images");
        }
        if(!favorisPictures.exists()){
            favorisPictures.createNewFile();


            String str1 = "[]";

            byte[] strToBytes = str1.getBytes();

            Files.write(Path.of(String.valueOf(favorisPictures)), strToBytes);

            System.out.println("Création du fichier images favori");
        }

        new SmartphoneShape(new WindowMenu());

    }

}

